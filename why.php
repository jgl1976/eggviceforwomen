﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">
		<div class="row">
<div class="col-xs-6 col-md-6"><span style="display=inline-block; font-size: X-large;">Why Egg Freezing:</span>
<br>
<span style="padding-left: 15px; display=inline-block;">
Oocyte Cryopreservation or egg freezing is the process of extracting, freezing and storing of a woman's eggs. Most importantly, egg freezing allows a woman to halt the ticking of her biological clock and bank her eggs for future use.
</span>
<span style="padding-left: 15px; display=inline-block;">
For many women living in the 21st century, the ticking of their biological clock does not fit the present day commitments they have planned for themselves. But, with the option to freeze eggs, women can focus on the present day commitments without the fear of regrets down the road.  A few of the reasons women are choosing to freeze their fertility are: 
</span>
<br><br><span style="display=inline-block; font-size: medium;"><strong>Medical:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">Egg freezing is an option for women diagnosed with cancer. It can give cancer patients an opportunity to preserve their healthy eggs prior to chemotherapy, radiation or surgery. Majority of the treatments used to treat cancer destroy a women's healthy eggs and can lead to infertility issues in the future. Furthermore, fertility preservation options vary depending upon the cancer, treatment plan, and age.    
</span>
</div>
  <div class="col-xs-6 col-md-6" ><img alt="" class="img-thumbnail" align="middle" src="img/why.png"></div>
  </div>
<div class="row">
<br>
<div class="col-xs-4 col-md-4"><span style="display=inline-block; font-size: medium;"><strong>Elective:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">There are various reasons you may choose to undergo the process of egg freezing. A few examples are; waiting to meet a life partner, putting your career first and receiving a higher degree in education. To be able to take the pressure off of yourself and halt the ticking of your biological clock is a life changing in many ways.    
</span></div>


<div class="col-xs-4 col-md-4"><span style="display=inline-block; font-size: medium;"><strong>Age:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">Age and female fertility are parallel to each other. As you age the quantity and quality of healthy eggs declines, which impacts your chances of conception each month. It’s important to know that it isn’t your age rather the age of your eggs that affects the ability to conceive. For example, if you freeze your eggs at 30-years-old, you will be just as successful as any other 30-year-old even if you don't use your eggs until you are 41-years-old. By freezing your eggs at 30-years old, you preserved the age of your eggs.  
</span></div>

<div class="col-xs-4 col-md-4"><span style="display=inline-block; font-size: medium;"><strong>Genetic:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">A genetic reason a woman would choose to freeze eggs would be a family history of early menopause. For example, if you have a family history of premature ovarian failure by freezing your eggs it can increase your chances of having a successful biological pregnancy later in life. </div>
</span></div>
</div>
<br>	<br>
<div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>
</body>
</html>
