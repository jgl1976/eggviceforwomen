﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="carousel slide" id="carousel-801101">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-801101">
					</li>
					<li data-slide-to="1" data-target="#carousel-801101">
					</li>
					<li data-slide-to="2" data-target="#carousel-801101">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="" class="img-thumbnail" src="img/car2.jpg">
						<div class="carousel-caption">
							<h4>
								Why Egg Freezing is Important
							</h4>
							<p>
						Most importantly, egg freezing allows a woman to halt the ticking of her biological clock and bank her eggs for future use.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" class="img-thumbnail" src="img/car1.jpg">
						<div class="carousel-caption">
							<h4>
								The Process of Egg Freezing
							</h4>
							<p>
								Depending upon the protocol that your physician has assigned to you, will depend on how long the entire egg freezing process will take from start to finish.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="" class="img-thumbnail" src="img/car3.jpg">
						<div class="carousel-caption">
							<h4>
								The Cost of Egg Freezing
							</h4>
							<p>
								Egg freezing is becoming more widespread as women learn they can preserve their fertility. 
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-801101" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-801101" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
			
		</div>
	</div>
	<div class="row clearfix"><br><br>
		<div class="col-md-4 column"  align="middle">
			<img alt="Right" src="img/hm_1.jpg" class="img-thumbnail">
			<h2>
				Why
			</h2>
			<p>Oocyte Cryopreservation or egg freezing is the process of extracting, freezing and storing of a woman's eggs. Most importantly, egg freezing allows a woman to halt the ticking of her biological clock and bank her eggs for future use.</p>
			<p>
				<a class="btn" href="why.php">View details »</a>
			</p>
		</div>
		<div class="col-md-4 column"  align="middle" >
			<img alt="Home" src="img/hm_2.jpg" class="img-thumbnail">
			<h2>
				Process
			</h2>
			<p>Depending upon the protocol that your physician has assigned to you, will depend on how long the entire egg freezing process will take from start to finish. But, generally speaking, it takes 4-6 weeks to complete an egg freezing cycle.</p>
			<p>
				<a class="btn" href="process.php">View details »</a>
			</p>
		</div>
		<div class="col-md-4 column"  align="middle">
			<img alt="Left" src="img/hm_mid.jpg" class="img-thumbnail">
			<h2>
				Cost
			</h2>
			<p>Egg freezing is becoming more widespread as women learn they can preserve their fertility. Along with awareness comes the intimidating breakdown of how much it costs. Although it is costly, it is the way to go when choosing to delay parenthood.</p>
			<p>
				<a class="btn" href="cost.php">View details »</a>
			</p>
		</div>
	</div><div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>
</body>
</html>
