﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">
		<div class="row">
<div class="col-xs-9 col-md-9"><span style="display=inline-block; font-size: X-large;">About:</span>
<br>
<span style="padding-left: 15px; display=inline-block;">
Bridget Brennan, RN, BSN, founder of EggviceforWomen created her educational organization in 2014 with the support of Main Line Fertility. After joining Main Line Fertility's team, Bridget saw early on the lack of education women have when it comes to fertility. She routinely heard a common phrase said by patients daily that had inspired her to start EggviceforWomen. Bridget says, "If I can reach one woman, I know I have succeeded."
</span><br><br>
<span style="padding-left: 15px; display=inline-block;">
Bridget graduated from Villanova University with her RN, BSN in 2008. After she had graduated, she, took her first nursing job at Inova Fairfax Hospital as a mother-baby nurse. After living in the District of Columbia area for two, she decided it was time to move back to the Philadelphia area. She took a position as a mother-baby nurse at the Hospital of the University of Pennsylvania. She worked at HUP for about a year when an opportunity presented itself at another local hospital. She decided to leave HUP at this time and took a mother-baby position at Lankenau Hospital. While at Lankenau Hospital, she was given an opportunity to work for Main Line Fertility as a fertility nurse. Bridget said, "It's my dream job, and I can't believe I have found my true passion in life."
</span>
</div>
  <div class="col-xs-3 col-md-3" ><img alt="" class="img-thumbnail" align="middle" src="img/about.jpg"></div>
  </div>
<br><br>
<div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>
</body>
</html>
