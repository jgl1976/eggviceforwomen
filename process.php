﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">

<div class="row">
<div class="col-xs-7 col-md-7"><span style="display=inline-block; font-size: X-large;">The Egg Freezing Process:</span><br>
<span style="padding-left: 15px; display=inline-block;">Depending upon the protocol that your physician has assigned to you, will depend on how long the entire egg freezing process will take from start to finish. But, generally speaking, it takes 4-6 weeks to complete an egg freezing cycle. 
</span><br><br>
<span style="display=inline-block; font-size: medium;"><strong>Ovarian Reserve Evaluation:</strong></span><br><br>
<span style="padding-left: 15px; display=inline-block;">The most accurate predictors of ovarian reserve available today the AMH (anti-Mullerian Hormone) and Day 3 FSH. When you get this blood test done, you will have a good indication of your egg quantity. 
</span><br><span style="padding-left: 15px; display=inline-block;">Anti-Mullerian Hormone- Anti-Mullerian Hormone or AMH is a substance produced by granulosa cells in the ovarian follicles. Women with higher AMH values most likely have a better success rate with ovarian stimulation, therefore, have more eggs retrieved giving a woman a higher pregnancy  success rate. 
</span><br><span style="padding-left: 15px; display=inline-block;">Follicle Stimulating Hormone- Follicle Stimulating Hormone or FSH measures the hormone produced by the pituitary gland that stimulates follicles to grow. The F SH hormone is measured on cycle day 3 of a woman's menstrual cycle to get an indication of her ovarian reserve. Increased levels of FSH may indicate poor ovarian reserve while low levels of FSH indicate greater ovarian reserve.  
</span><br><span style="padding-left: 15px; display=inline-block;">Both of these tests can and should be done yearly to keep you informed about your ovarian reserve and therefore your reproductive future.<br><br>
<span style="display=inline-block; font-size: medium;"><strong>Birth Control:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">Depending on the protocol assigned to you by your physician, he or she may use birth control to initiate the egg freezing process. A few reasons to use birth control is to suppress your natural cycle, synch you up for timeline purposes, or prevent over-stimulation of your ovaries  when started on hormone injections. 
</span>
</span>
</div>
<div class="col-xs-5 col-md-5"><button type="button" data-toggle="modal" data-target="#myModal"><img alt="Home" src="img/pro3.jpg"  width="348" height="516"  class="img-thumbnail"></button></div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" align="middle">
        <img alt="Home" src="img/pro3.jpg" class="img-thumbnail">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


</div>
<div class="row">

<div class="col-xs-12 col-md-12">
<span style="display=inline-block; font-size: medium;"><strong>Injectable Fertility Medication:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">Injectable fertility medications are used to stimulate the ovaries to help the ovaries recruit follicles. You will take these self-administered injectable medications for about 10 -12 days. The length of time you will take these medications depends on your response. During this time, you will get routine blood work and ultrasounds performed to monitor your hormone levels and the size of your follicles. Once your follicles have reached maturity, you will take your "trigger" injection. This injection contains hCG, and you will take it 36 hours prior to your retrieval. The purpose of this injection is to complete the final stage of the follicular growth.           
</span><br><br>
<span style="display=inline-block; font-size: medium;"><strong>Retrieval:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">Prior to your egg retrieval, you will be put under anesthesia in order to perform the procedure. Your doctor will then use ultrasound-guided needle to aspirate the mature eggs from the follicles. The egg retrieval process only takes about thirty minutes to complete.   Afterwards, you will be given discharge instructions. 
</span><br><br>
<span style="display=inline-block; font-size: medium;"><strong>The Process of Freezing Eggs:</strong></span><br>
<span style="padding-left: 15px; display=inline-block;">When all the eggs are retrieved, the embryologist assesses the eggs for maturity. Only mature eggs will be eligible for freezing since immature eggs do not fertilize. The mature eggs will be placed in solutions that will protect them during the freezing process. Once frozen the eggs are kept in liquid nitrogen until you ready to use them.         
</span>
</div>
</div>
<br>	
<div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>



</body>
</html>
