﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">
		<div class="row">
<div class="col-xs-12 col-md-12"><span style="padding-left: 15px; display=inline-block; font-size: X-large;">The Cost of Egg Freezing:</span>
<br>
<span style="padding-left: 15px; display=inline-block">
Egg freezing is becoming more widespread as women learn they can preserve their fertility. Along with awareness comes the intimidating breakdown of how much it costs and cost will most likely play a large role in your decision to freeze your eggs. Yes, egg freezing is an expensive procedure. Yes, it is true insurance companies do not cover this procedure because they consider it an elective procedure.  And yes, it is not a guaranteed that you will have a child from freezing your eggs-nothing in life is a 100% guaranteed. No, you cannot turn back the biological clock. So, make and investment now for your future family and remove the present day pressure from you. </span><br><br>
<span><strong>Sarah (age 34) Investment Banker, New York, NY</strong></span><br>
<span style="padding-left: 15px; display=inline-block">
“I decided to freeze my eggs because having a family has always been something that is extremely important to me. Unfortunately, I have not met the right person yet. I didn’t want to rush into a marriage solely to have children. Being able to freeze my eggs has taken the pressure off of me and has empowered me in more ways than I ever thought possible.  I would advise if you’re over 30 years old and single or involved in a fast track career you must consider this. I promise you it will change your life. You will never regret having frozen eggs, but you will regret not having preserved them when you had the chance.”</span>
<br><br>
<spanstyle="padding-left: 15px; display=inline-block">The costs for egg freezing are the same to those of routine IVF. The only difference between egg freezing and IVF is you delay fertilization and transfer for a future date. Main Line Fertility has an all-inclusive financial package for women choosing to preserve their fertility. Following is a breakdown of the costs:</span>
</div>

  </div><br><br>
<div class="row">

<div class="col-xs-12 col-md-12">

<table class="table table-hover">
  
  <tr class="info" align="middle">
    <td>&nbsp;Monitoring/Retrieval:</td>
    <td>&nbsp;$6,000</td>
  </tr>
  <tr class="info" align="middle">
    <td>&nbsp;Medications:</td>
    <td>&nbsp;$3,000-7,500</td>
  </tr>
  <tr class="info" align="middle">
    <td>&nbsp;Anesthesia:</td>
    <td>&nbsp;$500</td>
  </tr>
  <tr class="info" align="middle">
    <td>&nbsp;Yearly Storage Fee: </td>
    <td>&nbsp;$750 *first year storage fee is included in the order*</td>
  </tr>

</table>


</div>
<div class="col-xs-3 col-md-3">


</div>
</div>

<br>	<br>
<div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>
</body>
</html>
