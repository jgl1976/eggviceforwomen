﻿<?php
/* Script Created By Adam Khoury @ www.flashbuilding.com - March 17, 2010 */
$formMessage = "";
$senderName = "";
$senderEmail = "";
$senderMessage = "";
require_once "recaptchalib.php";
// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = "6LeMtP4SAAAAAMlstA0jYS5rUr9Qye7kjvr-wQio";
$secret = "6LeMtP4SAAAAANZuk0SQcqoUHr8vcH41FLNIj_DO";
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang = "en";
// The response from reCAPTCHA
$resp = null;
// The error code from reCAPTCHA, if any
$error = null;
$reCaptcha = new ReCaptcha($secret);
// Was there a reCAPTCHA response?

if ($_POST['button']) { // If the form is trying to post value of username field


   
    // Gather the posted form variables into local PHP variables
    $senderName = $_POST['username'];
    $senderEmail = $_POST['email'];
    $senderMessage = $_POST['msg'];
    // Make sure certain vars are present or else we do not send email yet
 	$resp = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
   	 );


 
    if (!$senderName || !$senderEmail || !$senderMessage || !$resp->success) {
 
         $formMessage = '<div class="alert alert-danger" role="alert">The form is incomplete, please fill in all fields.</div>';
 
    } else { // Here is the section in which the email actually gets sent

        // Run any filtering here
        $senderName = strip_tags($senderName);
        $senderName = stripslashes($senderName);
        $senderEmail = strip_tags($senderEmail);
        $senderEmail = stripslashes($senderEmail);
        $senderMessage = strip_tags($senderMessage);
        $senderMessage = stripslashes($senderMessage);
        // End Filtering
        $to = "jlevis76@gmail.com,bridge.brennan@gmail.com";         
        $from = "jlevis76@gmail.com";
        $subject = "You have a message from Your Website: Egg Vice for Women";
        // Begin Email Message Body
        $message = "
        $senderMessage

        $senderName
        $senderEmail
        ";
        // Set headers configurations
        $headers  = 'MIME-Version: 1.0' . "rn";
        $headers .= "Content-type: textrn";
        $headers .= "From: $fromrn";
        // Mail it now using PHP's mail function
        mail($to, $subject, $message, $headers);
        $formMessage = '<div class="alert alert-success" role="alert">Thanks, your message has been sent.</div>';
        $senderName = "";
        $senderEmail = "";
        $senderMessage = "";
    } // close the else condition

} // close if (POST condition
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EggviceforWomen</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing.">
<meta name="keywords" content="infertility, fertility, egg freezing, female, women, biological clock, career, life partner, education, oocyte cryopreservation, main line fertility, injectable medications,Anti-Mullerian Hormone,AMH, Follicle Stimulating Hormone, FSH, Birth Control, follicles,hormone,stimulation">
<meta name="author" content="WebITSO">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="img/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<!-- Font -->
<link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
<div class="container"><div class="col-md-12 column"><?php include_once "head.php"; ?></div><br>
	<div class="row clearfix">
		<div class="row">
  <div class="col-xs-12 col-md-12">
<form id="form1" name="form1" method="post" action="contact.php">
      <?php echo $formMessage; ?><br/>
  <br />Your Name:<br />
  <input name="username" type="text" id="username" class="form-control" placeholder="Enter your name" value="<?php echo $senderName; ?>" />
  <br />
  <br />
        Your Email:
  <br />
  <input name="email" type="text" class="form-control" id="email" placeholder="Enter email" value="<?php echo $senderEmail; ?>" />
  <br />
  <br />
        Your Message:
  <br />
  <textarea name="msg" id="msg" class="form-control" placeholder="Enter your message"><?php echo $senderMessage; ?></textarea>
  <br />
 <div class="g-recaptcha" data-sitekey="<?php echo $siteKey;?>"></div>
      <script type="text/javascript"
          src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang;?>">
      </script>
  <br />
  <input type="submit" name="button" id="button" class="btn btn-lg btn-primary" value="Send Now" /></form><br><br>
<div align="middle">
<address style="padding-left: 15px; display=inline-block"> <strong>Bryn Mawr Medical Arts Pavilion</strong><br> 825 Old Lancaster Road.
Suite 170 <br> Bryn Mawr, PA 19010-3121<br> <abbr title="Phone">P:</abbr> (484) 380-4892</address>
</div>

</div>

 
</div>
</div>
<br>	<br>
<div class="col-md-12 column"> <div class="navbar footbg">
	<div class="row clearfix"><br><br>
		<?php include_once "foot.php"; ?></div>
	</div></div>
</div>
</body>
</html>
