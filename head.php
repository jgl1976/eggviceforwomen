﻿<?php
$index = "";
$why = "";
$process = "";
$costs = "";
$about = "";
$contact = "";


$menulinkid = basename($_SERVER['PHP_SELF'],".php");
if ($menulinkid == "index"){
	  	$index = 'class="active"';
}else if ($menulinkid == "why"){
		$why = 'class="active"';
}else if ($menulinkid == "process"){
		$process = 'class="active"';
}else if ($menulinkid == "costs"){
		$costs = 'class="active"';
}else if ($menulinkid == "about"){
		$about = 'class="active"';
}else if ($menulinkid == "contact"){
		$contact = 'class="active"';
}
?>
<style type="text/css">
.navbar-header{
    margin-left:-45px !important;
}
</style>
<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="index.php"><img alt="Logo" src="img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;</a><span style="font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif; font-size: 32px;ns-serif;">Eggvice<span style="color:#df2b6a;">for</span>Women</span>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					
					<ul class="nav navbar-nav navbar-right">
						<li <?php echo $index;?>><a href="index.php">Home</a></li>		
						<li <?php echo $why;?>><a href="why.php">Why</a></li>
						<li <?php echo $process;?>><a href="process.php">Process</a></li>
						<li <?php echo $costs;?>><a href="costs.php">Costs</a></li>
						<li <?php echo $about;?>><a href="about.php">About</a></li>
						<li <?php echo $contact;?>><a href="contact.php">Contact</a></li>
					</ul>
				</div>
				
			</nav>