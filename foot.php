﻿<div class="col-md-4 column">
			<p class="text-muted text-left" style="color:white; padding-left: 15px; display=inline-block">
				<span style="font-weight: bold;" >Our Mission:</span><br>
EggviceforWomen is an educational organization associated with Main Line Fertility to help educate women about egg freezing. EggviceforWomen, inspired by a common phrase fertility patients say daily. This phrase is, “if I only knew.” This four-word phrase holds so much meaning, and with the help of EggviceforWomen, it is going to help future generations understand how close age and fertility are correlated. Our goal is to empower women to allow them to further their career, pursue additional education, or wait for the right partner without sacrificing their fertility. Making the best choice today allows you to have the best options tomorrow. 
			</p>
		</div>
		<div class="col-md-4 column">
			<ol class="list-unstyled" style="color:white; padding-left: 15px; display=inline-block">
				<li>
					<span style="font-weight: bold;" >Our Sponsors and Resources:</span>
				</li>
				<li>
					<a href="http://www.mainlinefertility.com/" style="color:white" target="_bank">Main Line Fertility</a>
				</li>
				<li>
					<a href="http://www.cnn.com/2012/10/22/health/frozen-egg-banks/" style="color:white" target="_bank">CNN: Egg freezing changing fertility treatments</a>
				</li>
				<li>
					<a href="http://www.nytimes.com/2011/09/01/fashion/fertility-is-a-matter-of-age-no-matter-how-young-a-woman-looks.html?pagewanted=all&_r=0" style="color:white" target="_bank">NY Times: Are You as Fertile as You Look?</a>
				</li>
				<li>
					<a href="http://www.phillymag.com/articles/fertility-the-new-facts-of-life/" style="color:white" target="_bank">Fertility: The new Facts of Life</a>
				</li>
				<li>
					<a href="http://yaledailynews.com/blog/2012/04/17/biological-clocks-ticking/" style="color:white" target="_bank">Yale News: Biological clocks ticking</a>
				</li>
				<li>
					<a href="https://www.asrm.org/splash/splash.aspx" style="color:white" target="_bank">American Society for Reproductive Medicine</a>
				</li>
				<li>
					<a href="http://www.advancedfertility.com/ivf.htm" style="color:white" target="_bank">Advanced Fertility Center of Chicago</a>
				</li>
				<li>
					<a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3987355/" style="color:white" target="_bank">The National Library of Medicine</a>
				</li>
				<li>
					<a href="http://www.forbes.com/sites/deniserestauri/2012/07/25/how-millennial-women-and-their-eggs-can-have-it-all/" style="color:white" target="_bank">How Millennial Women And Their Eggs Can Have It All</a>
				</li>
				
			</ol>
		</div>
		<div class="col-md-4 column" style="color:white; padding-left: 15px; display=inline-block">
			<blockquote class="pull-left">
				<p>"Freezing eggs empowers women. It allows women to take control of their future and gives them choices they may not have had in the past."</p> <small>Dr. <cite>Michael Glassner</cite></small>			</blockquote> 

<address style="color:white; padding-left: 15px; display=inline-block"> <strong>Bryn Mawr Medical Arts Pavilion</strong><br> 825 Old Lancaster Road.
Suite 170 <br> Bryn Mawr, PA 19010-3121<br> <abbr title="Phone">P:</abbr> (484) 380-4892</address>

		</div>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
 <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

